import os
import xlsxwriter

import bot_conf


def get_accounts(all_records):
    accounts = []
    for r in all_records:
        if r['account'] not in accounts:
            accounts.append(r['account'].replace("/", " "))
    return accounts


def get_records_by_accounts(all_records, only_category=None):
    only_category = "Office Buglance" if only_category in [":office", ":o", ":", ":f"] else only_category
    records_by_accounts = {k: [] for k in get_accounts(all_records)}
    for record in all_records:
        if not record['category'] == 'Last month remainder':
            account = record.pop('account').replace("/", " ")
            account_records = records_by_accounts[account]
            if not only_category:
                account_records.append(record)
            else:
                if record['category'] == only_category:
                    account_records.append(record)
            records_by_accounts.update({account: account_records})
    return records_by_accounts


def make_xlsx_from_csv(dir_of_file, filename, user_id, only_category=":all"):
    xlsx_dir = bot_conf.XLSX_DIR
    dir_for_output_file = f"{xlsx_dir}/{user_id}"

    # if dir is not present, create it
    if not os.path.exists(dir_for_output_file):
        os.makedirs(dir_for_output_file)

    lines_by_tokens = []
    # array of all lines of file
    with open(f"{dir_of_file}/{filename}", 'r', encoding='utf-8') as f:
        lines_of_file = f.read().split('\n')[1:-1]
        for line in lines_of_file:
            lines_by_tokens.append(line.split(";"))  # lines are divided by ';'

        for i, line in enumerate(lines_by_tokens):
            for j, token in enumerate(line):
                if token == '':
                    del lines_by_tokens[i][j]

    date = []
    account = []
    category = []
    amount = []
    description = []
    for line in lines_by_tokens:
        date.append(line[0])
        account.append(line[1])
        category.append(line[2])
        amount.append(line[3])
        if not line[-1] == "AZN":
            description.append(line[-1])
        else:
            description.append("No description")

    del lines_of_file, lines_by_tokens

    all_records = []

    for i in range(0, len(date)):
        record = {}
        record.update(date=date[i], account=account[i], category=category[i], amount=amount[i].replace(",", ""),
                      # remove commas from numbers like 1,500
                      description=description[i])
        all_records.append(record)

    del date, account, category, amount, description

    accounts = get_accounts(all_records)

    os.chdir(dir_for_output_file)

    all_records_by_accounts = get_records_by_accounts(all_records,
                                                      only_category if not only_category == ":all" else None)
    for k, account in enumerate(accounts):
        workbook = xlsxwriter.Workbook(f'{account}.xlsx')
        worksheet = workbook.add_worksheet('Expenses')

        records_of_account = all_records_by_accounts[account]
        all_income_and_expences = []

        for i, record in enumerate(records_of_account):
            worksheet.write(i, 0, record['date'])
            worksheet.write(i, 1, record['category'])
            worksheet.write(i, 2, float(record['amount']))
            worksheet.write(i, 3, record['description'])

            amount = str(record['amount'])
            if amount[0] == "-":
                all_income_and_expences.append(amount)
            else:
                all_income_and_expences.append("+" + amount)

        if all_income_and_expences:
            worksheet.write(len(records_of_account) + 1, 0, "Money left:")
            worksheet.write(len(records_of_account) + 1, 1, eval(" ".join(all_income_and_expences)))

        workbook.close()

    os.chdir("../..")

    return [f'{account}.xlsx' for account in accounts]


if __name__ == '__main__':
    make_xlsx_from_csv("./", "example.csv", 0)
