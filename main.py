# -*- coding: utf-8 -*-
import datetime
import logging
import os
import urllib.request

import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, ConversationHandler

import bot_conf
import money_manager_filter as mmf

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

FILE_RECEIVED, ENTER_CATEGORY = range(2)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text("""TURN OF carry over MODE IN SETTINGS OF MONEFY
    Waiting for csv file
    CHARACTER SET: UTF-8
    DECIMAL SEPARATOR: Decimal point '.'
    DELIMITER CHARACTER: Semicolon ';'""")
    return FILE_RECEIVED


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def cancel(update, context):
    update.message.reply_text('canceled')
    return ConversationHandler.END


def ask_category(update, context):
    update.message.reply_text("Enter category if you wanna get only one category. All or A or a for all categories")
    context.user_data['file_id'] = update.effective_message.document.file_id
    return ENTER_CATEGORY


def transform_into_xlsx(update, context):
    csv_dir = bot_conf.CSV_DIR
    tg_user_id = update.message.chat_id
    dir_for_file = f"{csv_dir}/{tg_user_id}"
    category = update.message.text if update.message.text not in ['All', 'A', "all", 'a'] else ":all"

    # if dir is not present, create it
    if not os.path.exists(dir_for_file):
        os.makedirs(dir_for_file)

    filename = datetime.datetime.now().strftime("%d-%m-%Y--%H-%M-%S") + ".csv"
    urllib.request.urlretrieve(context.bot.get_file(context.user_data['file_id']).file_path,
                               f"{dir_for_file}/{filename}")
    # try:
    filenames = mmf.make_xlsx_from_csv(dir_for_file, filename, tg_user_id,
                                           only_category=category)
    # except Exception as e:
    #     update.message.reply_text(f"Something went wrong, try again")
    #     return ConversationHandler.END

    return_file(update, context, filenames)
    return ConversationHandler.END


def return_file(update, context, filenames=None):
    filenames = context.user_data['filenames_to_return'] if not filenames else filenames
    user_id = update.message.from_user.id
    chat_id = update.effective_chat.id
    for filename in filenames:
        context.bot.send_document(chat_id=chat_id, document=open(bot_conf.XLSX_DIR + f"/{user_id}/{filename}", 'rb'))


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(token=bot_conf.BOT_TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start), MessageHandler(Filters.document, ask_category)],

        states={
            FILE_RECEIVED: [MessageHandler(Filters.document, ask_category)],

            ENTER_CATEGORY: [MessageHandler(Filters.regex('^(.*?)$'), transform_into_xlsx)],
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )
    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
